var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/commentDb";

exports.listCountries = function (req, res) {

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("assignment5Db");
    
    dbo.collection("countries").find({}).toArray(function(err, result) {
      db.close();        
      if (err) throw err;
  
      var countries = [];  
      for (var i = 0; i < result.length; i++) {
        countries.push(result[i].country);
      } 
      countries.sort();
      res.send(countries);  
      });
  }); 
};

exports.convert = function (req, res) {

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("assignment5Db");
    
    dbo.collection("countries").findOne({country:req.params.country1}, function(err, result) {
      if (err) throw err;
      var rate1 = result.rate;
  
      dbo.collection("countries").findOne({country:req.params.country2}, function(err, result) {
        db.close();
        if (err) throw err;
        var rate2 = result.rate;
        
        var new_amount = req.params.amount * rate1 / rate2;
        res.send([new_amount]);
      }); 
    });
  }); 
};

exports.insertCountry = function (req, res) {

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("assignment5Db");

    var ctry = {country: req.params.country, currency: req.params.currency, rate: req.params.rate};  
    console.log(ctry);
  
    dbo.collection("countries").insertOne(ctry, function(err, resp) {
      db.close();
      if (err) throw err;
    });
    
    res.send('One country inserted');
  }); 
};

exports.updateCountry = function (req, res) {

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("assignment5Db");

    var ctry = {country: req.params.country, currency: req.params.currency, rate: req.params.rate};  
    console.log(ctry);
  
    dbo.collection("countries").update({country: req.params.country}, ctry, function(err, resp) {
      db.close();
      if (err) throw err;
    });
    
    res.send('One country updated');
  }); 
};