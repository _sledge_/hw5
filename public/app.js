$(document).ready(function(){
    //=======clear textbox on page load ============================================================
    $("#amtbox").val('');
    $("#textbox").val('');
    $('#update_currency_field').val('');
    $('#update_rate_field').val('')
    
//    alert($("#select_1 option:selected").val());
    //========== get list of countries in exchange funds tab drop downs =============================
    
    $.ajax({
        url : "/listCountries",
        type : "GET",
        success : function(data){
            var len = data.length;
            //console.log(data);
            $(".dropdown").empty();
            
            $(".dropdown").append('<option value="" selected disabled hidden>Choose country</option>');
            
            for(var i =0; i<len;i++){
                $(".dropdown").append('<option value="'+data[i]+'">'+data[i]+'</option>');
            }
        }
    });
    
    
    
    // ============ toggle dropdown options on exchange funds tab ==========================
    
    $('#select_1').change(function(){
       $('#select_2 option:eq(' + $('#select_1').prop("selectedIndex") + ')').remove();
    });
    
    //============== change DOM to display (or not display) on tab click ====================
    
     $("#exchange").click(function(){
    	$(".exchange_content").show();
        $(".add_content").hide();
        $(".update_content").hide();
    });
    $("#add").click(function(){
    	$(".add_content").show();
        $(".exchange_content").hide();
        $(".update_content").hide();
    });
    $("#update").click(function(){
    	$(".update_content").show();
        $(".add_content").hide();
        $(".exchange_content").hide();
    });
    
    //=========== get amount, calculate, and display in exchange tab ======================
    
        $('#calc_button').click(function(){
            var urlstring ="/convert/"+$('#select_1 option:selected').text()+"/"
                   +$('#select_2 option:selected').text()+"/"
                   +$('#amtbox').val();
            //alert(urlstring);  

            
            $.ajax({
            url : urlstring,
            type : "GET",
            success : function(data){
                var str = $('#amtbox').val() + " ";
                str += $('#select_1 option:selected').text();
                str += " converts to " + '<span style="background-color: yellow;">' + data[0] + '</span>' + $('#select_2 option:selected').text();
                console.log(data[0]);
                $('.display_text').append(str);
            }
            }); 
        });

//==========update DB on click @ add tab =========================================
        $('#add_country_button').click(function(){
           var urlstring ="/insert/"+$('#country_field').val()+"/"
                   +$('#currency_field').val()+"/"
                   +$('#rate_field').val();
            //alert(urlstring);  

            
            $.ajax({
            url : urlstring,
            type : "GET",
            success : function(data){
                alert("Country Added!");
                }
            }); 
        });
        
//===============update DB @update tab ===========================================
        $('#update_changes_button').click(function(){
           var urlstring ="/update/"+$('#update_country_select_button option:selected').val()+"/"
                   +$('#update_currency_field').val()+"/"
                   +$('#update_rate_field').val();


            $.ajax({
            url : urlstring,
            type : "GET",
            success : function(data){
                alert("Country Updated!");
                }
            }); 
        });
}); 